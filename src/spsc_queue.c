#include "spsc_queue.h"

struct spsc_queue* spsc_queue_alloc(int capacity) {
	struct spsc_queue* queue = (struct spsc_queue*)
		malloc(sizeof(struct spsc_queue) + capacity * sizeof(T));

	queue->capacity = capacity;
	queue->head = 0;
	queue->tail = 0;

	return queue;
}

// No check for "overflow", because the code
// using this queue makes it impossible
void spsc_queue_push(struct spsc_queue* q, T elem) {
	q->buf[q->tail] = elem;
	q->tail = (q->tail + 1) % q->capacity;
}

T spsc_queue_poll(struct spsc_queue* q) {
	if (q->head == q->tail) {
		return spsc_queue_error;
	}

	T elem = q->buf[q->head];
	q->head = (q->head + 1) % q->capacity;
	return elem;
}

void spsc_queue_free(struct spsc_queue* q) {
	free(q);
}