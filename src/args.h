#ifndef __ORK_ARGS_H__
#define __ORG_ARGS_H__

#include <stdbool.h>
#include <stdint.h>
#include "units.h"

struct args {
	uint64_t urls_count;
	struct args_url {
		char* url;
		char* schema;
		char* host;
		uint64_t port;
		char* path;
		char* query;
	}* urls;

	char* method;
    char** headers;
    char* body;

	uint64_t threads;
	uint64_t connections;
	uint64_t duration;
	uint64_t ramp;
	uint64_t interarrival;
	uint32_t seed;
    bool pinthreads;
};

void usage();

int parse_args(struct args *args, int argc, char **argv);

uint64_t create_request(struct args args, int i, char* request);

void print_args(struct args args);

void free_args(struct args args);

#endif