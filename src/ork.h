#ifndef __CLIENT_H__
#define __CLIENT_H__

#define ORK_VERSION "0.1"

#define REQUEST_SIZE 4096
#define BODY_SIZE 1024
#define BUFFER_SIZE 1024

#endif /* __CLIENT_H__ */
