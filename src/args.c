#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <inttypes.h>
#include <getopt.h>

#include "http_parser.h"
#include "args.h"
#include "ork.h"

static struct option longopts[] = {
    { "threads",       required_argument, NULL, 't' },
    { "duration",      required_argument, NULL, 'd' },
    { "ramp",          required_argument, NULL, 'r' },
    { "interarrival",  required_argument, NULL, 'i' },
    { "connections",   required_argument, NULL, 'c' },
    { "seed",          required_argument, NULL, 's' },
    { "header",        required_argument, NULL, 'h' },
    { "body",          required_argument, NULL, 'b' },
    { "pinthreads",    no_argument,       NULL, 'y' },
    { "help",          no_argument,       NULL, 'H' },
    { "version",       no_argument,       NULL, 'v' },
    { NULL,            0,                 NULL,  0  }
};

void usage() {
    printf("Usage: ork <options> <url>                                    \n"
           "  Options:                                                    \n"
           "    -t, --threads      <N> Number of threads to use           \n"
           "    -d, --duration     <T> Duration of test                   \n"
           "    -r, --ramp         <T> Ramp (default 30s)                 \n"
           "    -i, --interarrival <U> Poisson inter-arrival time         \n"
           "    -c, --connections  <n> Number of connections per threads  \n"
           "    -s, --seed         <s> Seed for interarrival RNG          \n"
           "    -m, --method       <S> HTTP Method                        \n"
           "    -H, --header       <S> HTTP Header                        \n"
           "    -b, --body         <S> HTTP Request body                  \n"
           "    -y, --pinthreads       Pin threads to CPUs                \n"
           "    -v, --version          Print version details              \n"
           "                                                              \n"
           "  <N> Numeric arguments may include a SI unit (1k, 1M, 1G)    \n"
           "  <T> Time arguments may include a time unit (2s, 2m, 2h)     \n"
           "  <U> Microsecond-scale arguments (2us, 2ms, 2s)              \n");
}

static char *copy_url_part(char *url, struct http_parser_url *parts, enum http_parser_url_fields field) {
    char *part = NULL;

    if (parts->field_set & (1 << field)) {
        uint16_t off = parts->field_data[field].off;
        uint16_t len = parts->field_data[field].len;
        part = calloc(len + 1, sizeof(char));
        memcpy(part, &url[off], len);
        part[len] = '\0';
    }

    return part;
}

int parse_url(char* str, struct args_url* arg) {
    arg->url = str;
    arg->schema = NULL;
    arg->host = NULL;
    arg->port = 80;
    arg->query = "";

    struct http_parser_url parts;
    http_parser_url_init(&parts);
    if (http_parser_parse_url(arg->url, strlen(str), 0, &parts) ||
            !(parts.field_set & (1 << UF_SCHEMA)) ||
            !(parts.field_set & (1 << UF_HOST))) {
        fprintf(stderr, "invalid URL: %s\n", str);
        return 254;
    }

    arg->schema  = copy_url_part(arg->url, &parts, UF_SCHEMA);
    if (strncmp("http", arg->schema, 5) != 0) {
        fprintf(stderr, "Unsupported schema %s", arg->schema);
        return 253;
    }

    arg->host = copy_url_part(arg->url, &parts, UF_HOST);

    if (parts.field_set & (1 << UF_PORT)) {
        char* port_s = copy_url_part(arg->url, &parts, UF_PORT);
        arg->port = strtoul(port_s, NULL, 10);
        free(port_s);
    }

    if (parts.field_set & (1 << UF_PATH) &&
            parts.field_data[UF_PATH].len > 0) {
        arg->path = copy_url_part(arg->url, &parts, UF_PATH);
    }
    else {
        arg->path = calloc(2, sizeof(char));
        arg->path[0] = '/';
        arg->path[1] = '\0';
    }

    if (parts.field_set & (1 << UF_QUERY)) {
        arg->query = copy_url_part(arg->url, &parts, UF_QUERY);
    }
    else {
        arg->query = NULL;
    }

    return 0;
}

int parse_args(struct args *args, int argc, char **argv) {
    memset(args, 0, sizeof(struct args));
    args->urls_count = 0;
    args->urls = NULL;
    args->method = NULL;
    args->headers = calloc(argc, sizeof(char *));
    args->body = "";

    args->threads = 0;
    args->connections = 0;
    args->duration = 0;
    args->ramp = UINT64_C(30) * 1000000000;
    args->interarrival = 0;
    args->seed = time(NULL) + getpid();
    args->pinthreads  = false;

    char** header = args->headers;
    int c;
    while ((c = getopt_long(argc, argv, "t:d:r:i:c:s:m:H:b:yvh?", longopts, NULL)) != -1) {
        switch (c) {
            case 't':
                if (scan_metric(optarg, &args->threads)) return -1;
                break;
            case 'd': {
                uint64_t duration_s;
                if (scan_time(optarg, &duration_s)) return -1;
                args->duration = duration_s * 1000000000;
                break;
            }
            case 'r': {
                uint64_t ramp_s;
                if (scan_time(optarg, &ramp_s)) return -1;
                args->ramp = ramp_s * 1000000000;
                break;
            }
            case 'i': {
                uint64_t interarrival_us;
                if (scan_time_us(optarg, &interarrival_us)) return -1;
                args->interarrival = interarrival_us * 1000;
                break;
            }
            case 'c':
                args->connections = (uint64_t) strtoul(optarg, NULL, 10);
                break;
            case 's':
                args->seed = (uint32_t) strtoul(optarg, NULL, 10);
                break;
            case 'm':
                args->method = optarg;
                break;
            case 'H':
                *header = optarg;
                header++;
                break;
            case 'b' :
                args->body = optarg;
                break;
            case 'y' :
                args->pinthreads  = true;
                break;
            case 'v':
                printf("ork v%s\n", ORK_VERSION);
                printf("  Copyright 2018 Christopher Ferreira\n");
                printf("  Copyright (C) 2012 - Will Glozer\n");
                printf("  Copyright Joyent, Inc. and other Node contributors.\n");
                break;
            case 'h':
            case '?':
            case ':':
            default:
                return -1;
        }
    }
    *header = NULL;

    if (optind == argc
            || args->threads == 0
            || args->connections == 0
            || args->duration == 0
            || args->interarrival == 0) {
        return -1;
    }

    if (args->duration < args->ramp) {
        args->ramp = args->duration;
    }

    int urls_count = argc - optind;
    args->urls_count = urls_count;
    args->urls = malloc(urls_count * sizeof(struct args_url));
    for (int i = 0; i < urls_count; (optind++, i++)) {
        int ret = parse_url(argv[optind], args->urls + i);
        if (ret != 0) {
            return ret;
        }
    }

    return 0;
}


static uint64_t do_create_request(struct args args, int i, char* request, char* lineprefix) {
    struct args_url arg = args.urls[i];

    char request1[REQUEST_SIZE];
    char request2[REQUEST_SIZE];

    char* curr = request1;
    char* next = request2;

    if (arg.query != NULL) {
        sprintf(curr, "%s%s %s?%s HTTP/1.1\r\n%sHost: %s", lineprefix, args.method, arg.path, arg.query, lineprefix, arg.host);
    }
    else {
        sprintf(curr, "%s%s %s HTTP/1.1\r\n%sHost: %s", lineprefix, args.method, arg.path, lineprefix, arg.host);
    }

    char** header = args.headers;
    while (*header != NULL) {
        sprintf(next, "%s\r\n%s%s", curr, lineprefix, *header);
        header++;
        char* tmp;
        tmp = curr;
        curr = next;
        next = tmp;
    }

    uint64_t content_length = strlen(args.body);
    uint64_t len;
    if (content_length == 0) {
        len = sprintf(request, "%s\r\n\r\n", curr);
    }
    else {
        len = sprintf(request, "%s\r\n%sContent-Length: %lu\r\n\r\n%s", curr, lineprefix, content_length, args.body);
        char* tmp;
        tmp = curr;
        curr = next;
        next = tmp;
    }

    return len;
}

uint64_t create_request(struct args args, int i, char* request) {
    return do_create_request(args, i, request, "");
}

void print_args(struct args args) {
    char request[REQUEST_SIZE];
    char* duration_str = format_time_s(args.duration / 1000000000);
    char* ramp_str = format_time_s(args.ramp / 1000000000);
    char* interarrival_str = format_time_us(args.interarrival / 1000);

    printf("  Seed:           %u\n", args.seed);
    printf("  Urls: %ld\n", args.urls_count);
    for (uint64_t i = 0; i < args.urls_count; i++) {
        printf("    Url %2ld: %s\n", i + 1, args.urls[i].url);
        do_create_request(args, i, request, "      ");
        printf("    Request: \"\"\"\n%s\n    \"\"\"\n", request);
    }

    printf("  Duration:       %s\n", duration_str);
    printf("  Ramp:           %s\n", ramp_str);

    printf("  Threads:        %lu\n", args.threads);
    printf("    Interarrival: %s\n", interarrival_str);
    printf("    Connections:  %lu\n", args.connections);

    free(duration_str);
    free(ramp_str);
    free(interarrival_str);
}

void free_args(struct args args) {
    for (uint64_t i = 0; i < args.urls_count; i++) {
        free(args.urls[i].schema);
        free(args.urls[i].host);
        free(args.urls[i].path);
        free(args.urls[i].query);
    }
    free(args.urls);
    free(args.headers);
}
