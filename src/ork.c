#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <time.h>
#include <sys/sysinfo.h>

#include <hdr/hdr_histogram.h>
#include "http_parser.h"
#include "args.h"
#include "spsc_queue.h"
#include "ork.h"

#define EPOLL_WAIT_TIMEOUT_MS 100

#define HDR_HISTOGRAM_MAX INT64_C(5000000000)
#define HDR_HISTOGRAM_FIGURES 5

// Stores all experiment configuration.
struct args args;
// Common t0 value for throughput computation
struct timespec global_t0;
// Barrier used to wait for connections creation
pthread_barrier_t connect_barrier;
// Barrier used to synchronize just before the injection
pthread_barrier_t inject_barrier;

#define PADDING(s) uint8_t pad[128 - sizeof(s) % 128]

struct sender_data {
    // Url Index
    uint64_t url_idx;
    // Connections
    spsc_queue* queue;
    // Thread handle.
    pthread_t thread;
    // Start timestamp of the sender thread.
    uint64_t start;
    // Number of requests sent by sender threads.
    uint64_t nb_sent;
    // Done status for each couple of threads.
    bool* done;
};

struct sender_data_p {
    struct sender_data data;
    PADDING(struct sender_data);
};

struct receiver_data {
    // Server addr
    struct sockaddr_in server_addr;
    // Epoll file descriptor
    int epoll_fd;
    // Datas per connection
    struct conn_data* conn_datas;
    // Connections
    spsc_queue* queue;
    // Thread handle.
    pthread_t thread;
    // Number of requests sent by sender threads.
    uint64_t nb_received;
    // Done status for each couple of threads.
    uint64_t nb_per_status_code[6];
    // Done status for each couple of threads.
    bool done;
    // Ending timestamp of the receiver thread.
    uint64_t end;
    // HdrHistogram for latency
    struct hdr_histogram* histogram;
};

struct receiver_data_p {
    struct receiver_data data;
    PADDING(struct receiver_data);
};

struct conn_data {
    struct receiver_data* receiver_data;
    int fd;
    http_parser parser;
    uint64_t start;
    bool measure;
};

uint64_t time_now() {
    struct timespec time_now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &time_now);
    return (uint64_t) (
        (time_now.tv_sec - global_t0.tv_sec) * 1000000000
        + (time_now.tv_nsec - global_t0.tv_nsec));
}

enum sender_step {
    SENDER_STEP_RAMP_START,
    SENDER_STEP_MEASURE,
    SENDER_STEP_RAMP_END,
    SENDER_DONE
};

void *sender_main(void *opaque) {
    struct sender_data* data = (struct sender_data*) opaque;

    char request[REQUEST_SIZE];
    memset(request, 0, REQUEST_SIZE);
    int64_t len = create_request(args, data->url_idx, request);

    pthread_barrier_wait(&inject_barrier);

    data->start = time_now();
    uint64_t t1 = data->start;

    uint64_t stept0 = data->start;
    enum sender_step step = args.ramp > 0
        ? SENDER_STEP_RAMP_START
        : SENDER_STEP_MEASURE;

    while (step != SENDER_DONE) {
        struct conn_data* conn_data = spsc_queue_poll(data->queue);
        if (conn_data == NULL) {
            fprintf(stderr, "*** No more connection available\n");
            exit(40);
        }
        // Generate next request send time using Poisson distribution.
        uint64_t delay = log(RAND_MAX/(1.0 + rand())) * args.interarrival;
        uint64_t dl = t1 + delay;

        uint64_t time_diff = dl - stept0;
        switch (step) {
            case SENDER_STEP_RAMP_START:
                if (time_diff > args.ramp) {
                    stept0 = dl;
                    step = SENDER_STEP_MEASURE;
                }
                break;
            case SENDER_STEP_MEASURE:
                if (time_diff > args.duration) {
                    stept0 = dl;
                    step = args.ramp > 0
                        ? SENDER_STEP_RAMP_END
                        : SENDER_DONE;
                }
                break;
            case SENDER_STEP_RAMP_END:
                if (time_diff > args.ramp) {
                    stept0 = dl;
                    step = SENDER_DONE;
                }
                break;
            case SENDER_DONE:
                break;
        }

        // Fine grained busy waiting till we reach exact sending time.
        while (time_now() < dl);

        t1 = time_now();
        conn_data->start = t1;
        conn_data->measure = step == SENDER_STEP_MEASURE;
        data->nb_sent++;

        int64_t st = 0;
        do {
            int64_t rc = write(conn_data->fd, request + st, len - st);
            if (rc <= 0) {
                perror("Error sending packet");
                pthread_exit(NULL);
            }
            st += rc;
        } while (st < len);
    }

    *data->done = true;

    return NULL;
}

void prepare_connections(struct receiver_data* data) {
    int one = 1;

    for (uint64_t i = 0; i < args.connections; i++) {
        int fd;
        if ((fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP)) < 0) {
            perror("socket()");
            exit(3);
        }

        if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &one, sizeof(one)) < 0) {
            perror("setsockopt TCP_NODELAY\n");
            exit(4);
        }

        struct conn_data* conn_data = &data->conn_datas[i];
        conn_data->fd = fd;

        struct epoll_event event;
        event.data.fd = fd;
        event.data.ptr = conn_data;
        event.events = EPOLLOUT;
        if (epoll_ctl(data->epoll_fd, EPOLL_CTL_ADD, fd, &event) < 0) {
            perror("epoll_ctl(EPOLLOUT)");
            exit(5);
        }

        int rc = connect(fd, (struct sockaddr *) &data->server_addr, sizeof(data->server_addr));
        if (rc < 0 && errno != EINPROGRESS) {
            perror("connect()");
            exit(6);
        }
    }

    uint64_t pending = args.connections;
    while (pending > 0) {
        struct epoll_event event;
        if (epoll_wait(data->epoll_fd, &event, 1, -1) <= 0) {
            perror("Error epoll_wait(EPOLLOUT)");
            exit(30);
        }

        if ((event.events & EPOLLOUT) != EPOLLOUT) {
            if ((event.events & EPOLLERR) > 0 && errno == EINPROGRESS) {
                continue;
            }
            else {
                fprintf(stderr, "epoll_wait(EPOLLOUT): Invalid epoll events mask\n");
                if ((event.events & EPOLLERR) > 0) {
                    perror("EPOLLERR");
                }
                if ((event.events & EPOLLERR) != EPOLLERR) {
                    fprintf(stderr, "Invalid epoll events mask %d (hup: %s, err: %s)\n",
                        event.events,
                        (event.events & EPOLLHUP) ? "true" : "false",
                        (event.events & EPOLLERR) ? "true" : "false");
                }
                exit(31);
            }
        }

        struct conn_data* conn_data = (struct conn_data*) event.data.ptr;
        int fd = conn_data->fd;

        int flags = fcntl(fd, F_GETFL, 0);
        if (flags == -1) {
            perror("fnctl(F_GETFL)");
            exit(32);
        }

        if (fcntl(fd, F_SETFL, flags & ~O_NONBLOCK) == -1) {
            perror("fnctl(F_SETFL)");
            exit(33);
        }

        conn_data->receiver_data = data;
        http_parser_init(&conn_data->parser, HTTP_RESPONSE);
        conn_data->parser.data = conn_data;

        event.events = EPOLLIN;
        if (epoll_ctl(data->epoll_fd, EPOLL_CTL_MOD, fd, &event) < 0) {
            perror("epoll_ctl(EPOLLIN)");
            exit(34);
        }

        spsc_queue_push(data->queue, conn_data);
        pending--;
    }
}


int on_status(http_parser* parser, const char *at, size_t length) {
    struct conn_data* data = (struct conn_data*) parser->data;
    struct receiver_data* receiver_data = data->receiver_data;

    receiver_data->nb_received++;
    uint64_t status_range = parser->status_code / 100;
    if (status_range < 1 || status_range > 5) {
        status_range = 0;
    }
    receiver_data->nb_per_status_code[status_range]++;

    return 0;
}

int on_message_complete(http_parser* parser) {
    struct conn_data* data = (struct conn_data*) parser->data;
    struct receiver_data* receiver_data = data->receiver_data;

    // Read latency early
    uint64_t latency = time_now() - data->start;
    // Make the connection available to the sender thread
    spsc_queue_push(receiver_data->queue, data);
    // Store the latency
    if (data->measure) {
        hdr_record_value(receiver_data->histogram, latency);
    }

    return 0;
}

static struct http_parser_settings parser_settings = {
    .on_status = on_status,
    .on_message_complete = on_message_complete
};

void receive_loop(struct receiver_data* data) {
    int recv;
    char response[BUFFER_SIZE];

    while (!data->done) {
        struct epoll_event event;
        int ret = epoll_wait(data->epoll_fd, &event, 1, EPOLL_WAIT_TIMEOUT_MS);
        if (ret < 0) {
            perror("Error epoll_wait(EPOLLIN)");
            exit(40);
        }
        if (ret == 0) {
            continue;
        }

        if ((event.events & EPOLLIN) != EPOLLIN) {
            fprintf(stderr, "epoll_wait(EPOLLIN): Invalid epoll events mask\n");
            if ((event.events & EPOLLERR) > 0) {
                perror("EPOLLERR");
            }
            if ((event.events & EPOLLERR) != EPOLLERR) {
                fprintf(stderr, "Invalid epoll events mask %d (hup: %s, err: %s)\n",
                    event.events,
                    (event.events & EPOLLHUP) ? "true" : "false",
                    (event.events & EPOLLERR) ? "true" : "false");
            }
            exit(41);
        }

        struct conn_data* conn_data = event.data.ptr;
        recv = read(conn_data->fd, response, BUFFER_SIZE);
        if (recv < 0) {
            perror("Error reading packet");
            continue;
        }

        http_parser_execute(&conn_data->parser, &parser_settings, response, recv);
    }
}

void *receiver_main(void *opaque) {
    struct receiver_data* data = (struct receiver_data*) opaque;

    data->epoll_fd = epoll_create1(0);
    if (data->epoll_fd == -1) {
        perror("epoll_create1()");
        exit(2);
    }

    prepare_connections(data);

    pthread_barrier_wait(&connect_barrier);
    pthread_barrier_wait(&inject_barrier);

    receive_loop(data);

    data->end = time_now(global_t0);

    for (uint64_t i = 0; i < args.connections; i++) {
        close(data->conn_datas[i].fd);
    }
    close(data->epoll_fd);

    return NULL;
}

int prepare_thread_and_connections(struct sender_data_p* sender_datas, struct receiver_data_p* receiver_datas) {
    uint32_t servers[args.urls_count];

    for (uint64_t i = 0; i < args.urls_count; i++) {
        struct addrinfo hints, *servinfo;
        int rv;
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;

        if ((rv = getaddrinfo(args.urls[i].host, "http", &hints, &servinfo)) != 0) {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            exit(1);
        }

        struct sockaddr_in* in = (struct sockaddr_in*) servinfo->ai_addr;
        servers[i] = in->sin_addr.s_addr;
        freeaddrinfo(servinfo);
    }

    int num_cpus = sysconf(_SC_NPROCESSORS_ONLN);

    for (uint64_t idx = 0; idx < args.threads; idx++) {
        uint64_t url_idx = idx % args.urls_count;

        spsc_queue* queue = spsc_queue_alloc(args.connections * 2);
        struct sender_data* sender_data = &sender_datas[idx].data;
        sender_data->url_idx = url_idx;
        sender_data->queue = queue;
        sender_data->nb_sent = 0;

        struct receiver_data* receiver_data = &receiver_datas[idx].data;
        receiver_data->server_addr.sin_family = AF_INET;
        memcpy(&receiver_data->server_addr.sin_addr.s_addr,
               &servers[url_idx],
               sizeof(receiver_data->server_addr.sin_addr));
        receiver_data->server_addr.sin_port = htons(args.urls[url_idx].port);
        uint8_t* addr = (uint8_t*) &receiver_data->server_addr.sin_addr.s_addr;
        printf("  Thread %lu connecting to %u.%u.%u.%u:%lu\n",
               idx,
               addr[0], addr[1], addr[2], addr[3],
               args.urls[url_idx].port);

        receiver_data->queue = queue;
        receiver_data->conn_datas = calloc(args.connections, sizeof(struct conn_data));
        hdr_init(1, HDR_HISTOGRAM_MAX, HDR_HISTOGRAM_FIGURES, &receiver_data->histogram);

        sender_data->done = &receiver_data->done;

        if (pthread_create(&sender_data->thread, NULL, sender_main, sender_data)) {
            perror("Error creating sender thread");
            return 7;
        }

        if (pthread_create(&receiver_data->thread, NULL, receiver_main, receiver_data)) {
            perror("Error creating receiver thread");
            return 8;
        }

        if (args.pinthreads) {
            int sender_cpu = (idx * 2) % num_cpus;
            int receiver_cpu = sender_cpu + 1;
            cpu_set_t cpuset;

            CPU_ZERO(&cpuset);
            CPU_SET(sender_cpu, &cpuset);
            int s = pthread_setaffinity_np(sender_data->thread, sizeof(cpu_set_t), &cpuset);
            if (s != 0) {
                perror("Error setting thread affinity");
                return 9;
            }

            CPU_ZERO(&cpuset);
            CPU_SET(receiver_cpu, &cpuset);
            s = pthread_setaffinity_np(receiver_data->thread, sizeof(cpu_set_t), &cpuset);
            if (s != 0) {
               perror("Error setting thread affinity");
               return 10;
            }
        }
    }

    return 0;
}

void print_results(struct sender_data_p* sender_datas, struct receiver_data_p* receiver_datas) {
    uint64_t total_sent = 0;
    uint64_t total_received = 0;
    uint64_t total_per_status_code[6];
    for (int i = 0; i < 6; i++) {
        total_per_status_code[i] = 0;
    }
    uint64_t total_duration = 0;
    uint64_t throughput = 0;
    struct hdr_histogram* histogram;
    hdr_init(1, HDR_HISTOGRAM_MAX, HDR_HISTOGRAM_FIGURES, &histogram);
    for (uint64_t idx = 0; idx < args.threads; idx++) {
        total_sent += sender_datas[idx].data.nb_sent;
        total_received += receiver_datas[idx].data.nb_received;
        for (int i = 0; i < 6; i++) {
            total_per_status_code[i] += receiver_datas[idx].data.nb_per_status_code[i];
        }
        uint64_t diff_s = (receiver_datas[idx].data.end - sender_datas[idx].data.start) / 1000000000;
        total_duration += diff_s;
        if (diff_s > 0) {
            throughput += receiver_datas[idx].data.nb_received / diff_s;
        }

        hdr_add(histogram, receiver_datas[idx].data.histogram);
    }

    printf("  Sent:       %lu requests\n", total_sent);
    printf("  Received:   %lu requests\n", total_received);
    for (int i = 1; i < 6; i++) {
        if (total_per_status_code[i] > 0) {
            printf("    %dxx:      %lu requests\n", i, total_per_status_code[i]);
        }
    }
    if (total_per_status_code[0] > 0) {
        printf("    ???:      %lu requests\n", total_per_status_code[0]);
    }

    char* duration_s = format_time_s(total_duration / args.threads);
    printf("  Duration:   %s\n", duration_s);
    free(duration_s);
    printf("  Throughput: %lu requests/s\n", throughput);

    printf("  Latency (mean %.2lfms +-%.2lfms):\n",
            hdr_mean(histogram)   / 1000000,
            hdr_stddev(histogram) / 1000000);

    printf("    Min:  %10.2lfms\n",  (double) hdr_min(histogram) / 1000000);
    printf("    50%%:  %10.2lfms\n", (double) hdr_value_at_percentile(histogram,  5) / 1000000);
    printf("    75%%:  %10.2lfms\n", (double) hdr_value_at_percentile(histogram, 75) / 1000000);
    printf("    95%%:  %10.2lfms\n", (double) hdr_value_at_percentile(histogram, 95) / 1000000);
    printf("    99%%:  %10.2lfms\n", (double) hdr_value_at_percentile(histogram, 99) / 1000000);
    printf("    Max:  %10.2lfms\n",  (double) hdr_max(histogram) / 1000000);

}

static void* calloc_padded(size_t nmemb, size_t size, size_t padding) {
    void* ptr = malloc(nmemb * size + padding);
    bzero(ptr, nmemb * size + padding);
    return ptr;
}

static void* memalign(void* ptr, size_t align) {
    uintptr_t iptr = (uintptr_t) ptr;
    uintptr_t diff = iptr % align;
    return (void*) ((diff == 0) ? iptr : iptr + align - diff);
}

int main(int argc, char * argv[]) {
    if (parse_args(&args, argc, argv)) {
        usage();
        free_args(args);
        return 255;
    }

    printf("# Starting ork\n");
    print_args(args);

    printf("# Preparing threads and connections\n");

    void* freeme1 = calloc_padded(args.threads, sizeof(struct sender_data_p), 127);
    struct sender_data_p* sender_datas = (struct sender_data_p*) memalign(freeme1, 128);

    void* freeme2 = calloc_padded(args.threads, sizeof(struct receiver_data_p), 127);
    struct receiver_data_p* receiver_datas = (struct receiver_data_p*) memalign(freeme2, 128);

    pthread_barrier_init(&connect_barrier, NULL, args.threads + 1);
    pthread_barrier_init(&inject_barrier, NULL, args.threads * 2 + 1);

    srand(args.seed);

    int errcode = prepare_thread_and_connections(sender_datas, receiver_datas);
    if (errcode > 0) {
        for (uint64_t idx = 0; idx < args.threads; idx++) {
            free(receiver_datas[idx].data.conn_datas);
            spsc_queue_free(sender_datas[idx].data.queue);
        }
        free(freeme1);
        free(freeme2);
        free_args(args);
        return errcode;
    }

    pthread_barrier_wait(&connect_barrier);
    printf("# Starting load injection\n");
    pthread_barrier_wait(&inject_barrier);

    // Wait for all threads to terminate.
    for (uint64_t idx = 0; idx < args.threads; idx++) {
        pthread_join(sender_datas[idx].data.thread, NULL);
    }
    printf("  Sender   threads finished\n");
    for (uint64_t idx = 0; idx < args.threads; idx++) {
        pthread_join(receiver_datas[idx].data.thread, NULL);
    }
    printf("  Receiver threads finished\n");

    printf("# Results\n");
    print_results(sender_datas, receiver_datas);

    for (uint64_t idx = 0; idx < args.threads; idx++) {
        free(receiver_datas[idx].data.conn_datas);
        spsc_queue_free(sender_datas[idx].data.queue);
        hdr_close(receiver_datas[idx].data.histogram);
    }
    free(freeme1);
    free(freeme2);
    free_args(args);

    return EXIT_SUCCESS;
}
