#ifndef __SPSC_QUEUE__
#define __SPSC_QUEUE__

#include <stdlib.h>
#include <stdint.h>

struct sock_data;
typedef struct conn_data* T;
#define spsc_queue_error NULL;

typedef struct spsc_queue {
	uint64_t capacity;
	uint64_t head;
	uint64_t tail;
	T buf[];
} spsc_queue;

struct spsc_queue* spsc_queue_alloc(int capacity);

void spsc_queue_push(struct spsc_queue* q, T elem);

T spsc_queue_poll(struct spsc_queue* q);

void spsc_queue_free(struct spsc_queue* q);

#endif