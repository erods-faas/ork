SRC=$(wildcard src/*.c)
DEP=$(patsubst src/%.c, build/%.d, $(SRC))
OBJ=$(patsubst src/%.c, build/%.o, $(SRC))

CC=gcc
CFLAGS=-std=c11 -Wall -Wextra -Wno-unused-parameter -g -fno-omit-frame-pointer -D_GNU_SOURCE
LDLIBS=-pthread -lrt -lm -lhdr_histogram

all: ork

$(DEP): build/%.d: src/%.c
	@mkdir -p build
	echo -n 'build/' >$@
	$(CC) $(CFLAGS) -MM $^ >>$@

$(OBJ): build/%.o: src/%.c
	@mkdir -p build
	$(CC) $(CFLAGS) -c $< -o $@

include $(DEP)

ork: $(OBJ)
	$(CC) -o $@ $^ $(LDLIBS)

.PHONY: clean
clean: 
	rm -rf build
	rm -f ork
	make -C hdrhistogram clean
